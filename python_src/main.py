from lib.wifi_manager import WifiManager
import utime
from machine import Pin, RTC, I2C, ADC
from neopixel import NeoPixel
import network, ntptime

from lib.makerverse_RV3028 import Makerverse_RV3028
from lib.rgbled import RgbLed

""" Const definition """
LED_COUNT = 144

""" Pin configuration """
PICO_LED_PIN = 'LED'
RGB_LED_PIN = 13

PROG_BUTTON_PIN = 17
USER_BUTTON_1_PIN = 15
USER_BUTTON_2_PIN = 17

I2C_SDA_PIN = 4
I2C_SCL_PIN = 5

RTC_INT_PIN = 6
RTC_CLK_PIN = 7

INPUT_VOLTAGE_PIN = 26

""" Pin initialization """
picoLED = Pin(PICO_LED_PIN, mode=Pin.OUT)

progButton = Pin(PROG_BUTTON_PIN, mode=Pin.IN, pull=Pin.PULL_UP)
# progButton.irq(trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING, handler=callback)

userButton1 = Pin(USER_BUTTON_1_PIN, mode=Pin.IN, pull=Pin.PULL_UP)
# userButton1.irq(trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING, handler=callback)

userButton2 = Pin(USER_BUTTON_2_PIN, mode=Pin.IN, pull=Pin.PULL_UP)
# userButton2.irq(trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING, handler=callback)

i2c = I2C(0, scl=Pin(I2C_SCL_PIN), sda=Pin(I2C_SDA_PIN), freq=400_000)

rtcInt = Pin(RTC_INT_PIN, mode=Pin.IN, pull=Pin.PULL_UP)
# rtcInt.irq(trigger=Pin.IRQ_FALLING, handler=callback)

rtcClk = Pin(RTC_CLK_PIN, mode=Pin.IN, pull=Pin.PULL_UP)
# rtcClk.irq(trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING, handler=callback)

inputVoltage = ADC(INPUT_VOLTAGE_PIN)

""" external RTC initialization """
externalRtc = Makerverse_RV3028(i2c=i2c)

""" LED initialization """
rgbLed = RgbLed(count=LED_COUNT, data=RGB_LED_PIN)


DATETIME_ELEMENTS = {
    "year": 0,
    "month": 1,
    "day": 2,
    "day_of_week": 3,
    "hour": 4,
    "minute": 5,
    "second": 6,
    "millisecond": 7
}

# set an element of the RTC's datetime to a different value
def set_datetime_element(rtc, datetime_element, value):
    date = list(rtc.datetime())
    date[DATETIME_ELEMENTS[datetime_element]] = value
    rtc.datetime(date)

LED = 6 

# Example of usage
pin = Pin(13, Pin.OUT)   # set GPIO0 to output to drive NeoPixels
np = NeoPixel(pin, LED)   # create NeoPixel driver on GPIO0 for 8 pixels
np[0] = (255, 255, 255) # set the first pixel to white
np.write()              # write data to all pixels
wm = WifiManager()
wm.connect()
rtc = RTC()

while True:
    ntptime.settime()
    set_datetime_element(rtc, "hour", 22) # I call this to change the hour to 8am for me
    date = list(rtc.datetime())
    process = date[4] * 60 + date[5]
    processInP = process / 1439 * 100
    ledInP = LED
    processProjection = processInP / 100 * ledInP
    print(processProjection)
    
    for x in range(LED):
        if processProjection >= 1.0:
            np[x] = (0, 100, 0)
            processProjection -= 1.0
        else:
            np[x] = (0, int(processProjection * 100), 0)
            processProjection = 0
    np.write() 
    if wm.is_connected():
        print('Connected!')
    else:
        print('Disconnected!')
    utime.sleep(60)
