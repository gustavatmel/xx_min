# To run, open the KiCad scripting console and type: exec(open('development/my_projects/xx_min/01_hardware/helpers/led_array_place.py').read())
# 
# This script will re-position D[1-144] into a matrix filling the specified width and height, at the location provided below.
# After running you will have to press F11/F12 to force the screen to re-render.

import sys
from pcbnew import *
pcb = GetBoard()


initialX = FromMM(175)
initialY = FromMM(30)

spacingX = FromMM(10)
spacingY = FromMM(10)

print(f'Spacing X,Y = {spacingX},{spacingY}')


nCount = 1


print('Start Place')

for y in range(24):
    for x in range(6):
        Ref = f'D{nCount}'
        nCount = nCount + 1
        print(Ref)
        try:
            nPart = pcb.FindFootprintByReference(Ref)
            nPart.SetPosition(wxPoint(initialX + x*spacingX, initialY + y*spacingY))  # Update XY

            pad = nPart.FindPadByNumber('1')
            padPosition = pad.GetPosition()
            track = PCB_TRACK(pcb)
            track.SetStart(wxPoint(padPosition[0], padPosition[1]))
            track.SetEnd(wxPoint(padPosition[0]+Millimeter2iu(2), padPosition[1]))
            track.SetWidth(Millimeter2iu(0.5))
            pcb.Add(track)

            via = PCB_VIA(pcb)
            via.SetPosition(wxPoint(padPosition[0]+Millimeter2iu(2), padPosition[1]))
            via.SetDrill(Millimeter2iu(0.4))
            via.SetWidth(Millimeter2iu(0.8))
            pcb.Add(via)

        except:
            print('wtf')
        
Refresh()        
print('Finished Place')

