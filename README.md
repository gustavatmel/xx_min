# xx_min

## setup
load firmware to raspberry pi pico w
download from: https://micropython.org/download/rp2-pico-w/

## load micropython files to pico w
```shell
python microupload.py -v -C ./python_src /dev/ttyACM0 .  
```

## debug and uploa c/c++ files using CLion
### with picoprobe

```
source [find interface/picoprobe.cfg]
transport select swd
adapter speed 5000

# CLion Fix
proc init_board  { } {
gdb_port 3333;
}

source [find target/rp2040.cfg]
```

### with adafruit ftdi
https://github.com/raspberrypi/openocd/issues/11
