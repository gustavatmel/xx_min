#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/cyw43_arch.h"
extern "C" {
    #include "dhcpserver.h"
}
#include "hardware/i2c.h"
#include "hardware/pio.h"

#include <PicoLed.hpp>
#include <Effects/Marquee.hpp>
#include <Effects/Stars.hpp>
#include <Effects/Comet.hpp>
#include <Effects/Bounce.hpp>
#include <Effects/Particles.hpp>


// I2C defines
// This example will use I2C0 on GPIO8 (SDA) and GPIO9 (SCL) running at 400KHz.
// Pins can be changed, see the GPIO function select table in the datasheet for information on GPIO assignments
#define I2C_PORT i2c0
#define I2C_SDA 4
#define I2C_SCL 5

#define LED_PIN 13
#define LED_LENGTH 144

int main()
{
    stdio_init_all();

    // I2C Initialisation. Using it at 400Khz.
    i2c_init(I2C_PORT, 400*1000);
    
    gpio_set_function(I2C_SDA, GPIO_FUNC_I2C);
    gpio_set_function(I2C_SCL, GPIO_FUNC_I2C);
    gpio_pull_up(I2C_SDA);
    gpio_pull_up(I2C_SCL);

    sleep_ms(5000);

    // 0. Initialize LED strip
    printf("0. Initialize LED strip\n");
    auto ledStrip = PicoLed::addLeds<PicoLed::WS2812B>(pio0, 0, LED_PIN, LED_LENGTH, PicoLed::FORMAT_GRB);
    ledStrip.setBrightness(64);
    printf("1. Clear the strip!\n");


    // 2. Set all LEDs to green!
    printf("2. Set all LEDs to green!\n");
    ledStrip.fill( PicoLed::RGB(0, 16, 0) );
    ledStrip.show();

    printf("Hello, world!\n");
    sleep_ms(1000);

    ledStrip.clear();
    ledStrip.show();


    // WIFI
    cyw43_arch_init();

    cyw43_arch_enable_ap_mode("xx_min_setup", "1234qwer.", CYW43_AUTH_WPA2_AES_PSK);


    ip4_addr_t gw, mask;
    IP4_ADDR(&gw, 192, 168, 4, 1);
    IP4_ADDR(&mask, 255, 255, 255, 0);

    // Start the dhcp server
    dhcp_server_t dhcp_server;
    dhcp_server_init(&dhcp_server, &gw, &mask);

    while (true) {
        printf("Hello, world!\n");
        sleep_ms(1000);

    }
    return 0;
}
